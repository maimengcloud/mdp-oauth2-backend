//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.mdp;

import com.mdp.oauth2.server.integration.IntegrationFilter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.DefaultLoginPageConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@EnableWebSecurity
public class OAuth2AuthorizationServerSecurity extends WebSecurityConfigurerAdapter {
    public OAuth2AuthorizationServerSecurity() {
    }

    protected void configure(HttpSecurity http) throws Exception {

        http.apply(new DefaultLoginPageConfigurer<>());
        http.formLogin(x->x.usernameParameter("userloginid"));
        applyDefaultConfiguration2(http);
    }

    public static void applyDefaultConfiguration2(HttpSecurity http) throws Exception {
        http.addFilterBefore(new IntegrationFilter(), UsernamePasswordAuthenticationFilter.class);
        OAuth2AuthorizationServerConfigurer<HttpSecurity> authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer();

        RequestMatcher[] endpointMatchers = (RequestMatcher[])authorizationServerConfigurer.getEndpointMatchers().toArray(new RequestMatcher[0]);
        http.authorizeRequests().requestMatchers(new OrRequestMatcher(endpointMatchers)).authenticated().and()
        .csrf((csrf) -> {
            csrf.ignoringRequestMatchers(endpointMatchers);
        }).apply(authorizationServerConfigurer);
    }
}

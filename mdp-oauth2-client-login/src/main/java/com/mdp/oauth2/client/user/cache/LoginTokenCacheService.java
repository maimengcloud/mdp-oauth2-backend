package com.mdp.oauth2.client.user.cache;

import com.mdp.core.api.CacheHKVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 切换用户的时候，免密登录，需要先把重要信息通过redis传递，防止接口暴露关键信息
 */
@Service
public class LoginTokenCacheService {
    @Autowired
    CacheHKVService redisTemplate;
    public static String CACHE_KEY="TOKEN_CACHE:";

    public void put(String userid,String token){
        redisTemplate.setValue(CACHE_KEY+userid,token,2, TimeUnit.MINUTES);
    }
    public String get(String userid){
        return (String) redisTemplate.getValue(CACHE_KEY+userid);
    }
}

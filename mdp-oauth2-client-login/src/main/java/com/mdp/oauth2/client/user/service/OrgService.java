package com.mdp.oauth2.client.user.service;

import com.mdp.core.entity.Tips;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OrgService {
    @Autowired
    MemMemberService memMemberService;

   public Tips updateOrgEmail(Map<String,Object> params){
       return memMemberService.updateOrgEmail(params);
    }
}

package com.mdp.oauth2.client.user.service;

import com.mdp.core.service.BaseService;
import com.mdp.oauth2.client.user.entity.ValidCode;
import com.mdp.oauth2.client.user.entity.ValidCodeVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com  顶级模块 mk 大模块 mem 小模块 <br>
 * 实体 MemberValidCode 表 MK.mem_member_valid_code 当前主键(包括多主键): id; 
 ***/
@Service
public class ValidCodeUtilService  {

	@Autowired
	ValidCodeService validCodeService;


	static Logger logger =LoggerFactory.getLogger(ValidCodeUtilService.class);

	public void updateSomeFieldByPk(ValidCodeVo validCodeVoUpdate) {
			validCodeService.updateSomeFieldByPk(validCodeVoUpdate);
	}

	public void insert(ValidCodeVo validCodeVo) {
			validCodeService.insert(validCodeVo);
	}

	public List<ValidCodeVo> selectListByWhere(ValidCodeVo vcVo) {
			List<ValidCode> datas=validCodeService.selectListByWhere(vcVo);
			if(datas==null){
				return null;
			}
			return datas.stream().map(k->dto(k,vcVo.getUserType())).collect(Collectors.toList());
	}

	private ValidCodeVo dto(ValidCode k,String userType) {
		ValidCodeVo validCodeVo=new ValidCodeVo();
		BeanUtils.copyProperties(k,validCodeVo);
		validCodeVo.setUserType(userType);
		return validCodeVo;
	}

	public String createKey(String id) {
		return validCodeService.createKey("id");
	}
	/** 请在此类添加自定义函数 */

}


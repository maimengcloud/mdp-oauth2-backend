/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mdp.oauth2.client.user.ctrl;

import com.mdp.core.entity.Result;
import com.mdp.oauth2.client.user.entity.RequestTokenParams;
import com.mdp.core.entity.Tips;
import com.mdp.core.utils.BaseUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.MapOAuth2AccessTokenResponseConverter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Joe Grandja
 * @since 0.0.1
 */

@Controller
public class TokenCredentialsController {

	@Autowired
	ClientRegistrationRepository clientRegistrationRepository;

	MapOAuth2AccessTokenResponseConverter converter=new MapOAuth2AccessTokenResponseConverter();


	/**
	 * credentials 模式获取token
	 *
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/login/token", params = {"grantType=client_credentials"})
	public String clientCredentialsGrantWeb(Model model, @RequestParam Map<String,Object> params , RedirectAttributes attributes) {
		RequestTokenParams requestTokenParams=BaseUtils.fromMap(params,RequestTokenParams.class);
		Map<String,Object> result=this.clientCredentialsGrant(params,requestTokenParams);
		String redirectUri= (String) params.get("redirectUri");
		OAuth2AccessTokenResponse response= ((OAuth2AccessTokenResponse)(result.get("data")));

		if(StringUtils.hasText(redirectUri)){
			attributes.addAttribute("accessToken",response.getAccessToken().getTokenValue());
			User user= LoginUtils.getCurrentUserInfo();
			attributes.addAttribute("userid",user.getUserid());
			attributes.addAttribute("username",user.getUsername());
			return "redirect:"+redirectUri;
		}else {
			model.addAllAttributes(result);
			return "index";
		}
	}
	/**
	 * credentials 模式获取token
	 * @param webparams
	 * @return
	 */
	@PostMapping(value = "/login/token", params = "grantType=client_credentials")
	public Result clientCredentialsGrant( @RequestParam Map<String,Object> webparams , @RequestBody RequestTokenParams paramsVo ) {
		Tips tips = new Tips("成功");
		try {
			ClientRegistration client=clientRegistrationRepository.findByRegistrationId("client-login");
			String clientId= paramsVo.getClientId();
			String clientSecret= paramsVo.getClientSecret();
			String registration_id= paramsVo.getRegistrationId();
			if(StringUtils.hasText(registration_id)){
				client=clientRegistrationRepository.findByRegistrationId(registration_id);
			}
			if(client==null){
				return Result.error("client-id-not-exists","客户端不存在");
			}

			Map<String,Object> params=new HashMap<>();
			//params.put("client_id",client_id);//userid,openid,phoneno,email等
			//params.put("client_secret",client_secret);//密码，短信验证码，微信登陆时对应的code等
			params.put("grantType","client_credentials");
			ClientRegistration finalClient = client;
			Map messages = WebClient.create()
					.post()
					.uri(client.getProviderDetails().getTokenUri()+"?grant_type={grantType}",params)
					.headers(h->{
						h.add("Authorization","Basic "+ Base64.getEncoder().encodeToString((finalClient.getClientId()+":"+ finalClient.getClientSecret()).getBytes(Charset.forName("UTF-8"))));
					})
					.retrieve()
					.bodyToMono(Map.class)
					.block();

			OAuth2AccessTokenResponse response=converter.convert(messages);
			return Result.ok().setData(response);
		}catch (Exception e){
			return Result.error(e.getMessage());
		}

	}

}

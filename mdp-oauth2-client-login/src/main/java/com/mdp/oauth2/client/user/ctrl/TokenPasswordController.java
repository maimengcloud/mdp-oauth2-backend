/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mdp.oauth2.client.user.ctrl;

import com.mdp.core.api.Sequence;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.utils.LogUtils;
import com.mdp.core.utils.ObjectTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.oauth2.client.user.cache.LoginTokenCacheService;
import com.mdp.oauth2.client.user.entity.RequestTokenParams;
import com.mdp.oauth2.client.user.service.SmsLoginService;
import com.mdp.core.entity.Tips;
import com.mdp.core.utils.BaseUtils;
import com.mdp.oauth2.client.user.util.LockUtil;
import com.mdp.qx.HasRole;
import com.mdp.safe.client.dict.AuthType;
import com.mdp.safe.client.entity.CommonUserDetails;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.jwt.JwtLocalLoginService;
import com.mdp.safe.client.pwd.SafePasswordEncoder;
import com.mdp.safe.client.service.MdpLoginQueueService;
import com.mdp.safe.client.utils.LoginUtils;
import nl.bitwalker.useragentutils.Browser;
import nl.bitwalker.useragentutils.OperatingSystem;
import nl.bitwalker.useragentutils.UserAgent;
import nl.bitwalker.useragentutils.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.MapOAuth2AccessTokenResponseConverter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.*;

/**
 * @author Joe Grandja
 * @since 0.0.1
 */

@Controller
public class TokenPasswordController {

	@Autowired
	ClientRegistrationRepository clientRegistrationRepository;

	@Autowired
	JwtLocalLoginService jwtLoginService;

	@Autowired
	SmsLoginService smsLoginService;

	@Autowired
	LoginTokenCacheService tokenCacheService;

	WebClient webClient=WebClient.create();

	MapOAuth2AccessTokenResponseConverter converter=new MapOAuth2AccessTokenResponseConverter();

	Logger logger= LoggerFactory.getLogger(TokenPasswordController.class);


	@Autowired
	MdpLoginQueueService loginRecordService;

	@Autowired
	Sequence sequenceService;

	/**
	 * 密码登陆
	 * 支持手机、邮箱、微信等多种方式登陆
	 *
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/login/token", params = {"grantType=password"})
	public String clientPasswordGrantWeb(Model model, @RequestParam Map<String,Object> params, RedirectAttributes attributes ) {
		RequestTokenParams requestTokenParams=BaseUtils.fromMap(params,RequestTokenParams.class);
		Map<String,Object> result=this.clientPasswordGrant(params,requestTokenParams);
		Tips tips= (Tips) result.get("tips");
		if(tips.isOk()){
			String redirectUri= (String) params.get("redirectUri");
			OAuth2AccessTokenResponse response= ((OAuth2AccessTokenResponse)(result.get("data")));
			if(StringUtils.hasText(redirectUri)){
				attributes.addAttribute("accessToken",response.getAccessToken().getTokenValue());
				User user= LoginUtils.getCurrentUserInfo();
				attributes.addAttribute("userid",user.getUserid());
				attributes.addAttribute("username",user.getUsername());
				return "redirect:"+redirectUri;
			}else {
				model.addAllAttributes(result);
				return "index";
			}
		}else{
			model.addAllAttributes(result);
			return "";
		}

	}
	/**
	 * 密码登陆
	 * 支持手机、邮箱、微信等多种方式登陆
	 *
	 * @param webparams
	 * @return
	 */
	@PostMapping(value = "/login/token", params = "grantType=password")
	@ResponseBody  public Result clientPasswordGrant(@RequestParam Map<String,Object> webparams , @RequestBody RequestTokenParams paramsVo) {
		Tips tips = new Tips("成功");
		try {
			ClientRegistration client=clientRegistrationRepository.findByRegistrationId("client-login");
			String authType= paramsVo.getAuthType();
			if(!StringUtils.hasText(authType)){
				authType="password";
			}
			String userid= paramsVo.getUserid();
			String userloginid= paramsVo.getUserloginid();
			String password= paramsVo.getPassword();

			String userType= (String) webparams.get("userType");
			String registrationId= paramsVo.getRegistrationId();
			if(AuthType.sms.name().equals(authType)){
				tips=smsLoginService.checkPhoneCode(userloginid,password);
				if(!tips.isOk()){
					return Result.error(LangTips.fromTips(tips));
				}
			}
			if(AuthType.switch_user_by_token.name().equals(authType)){
				if(LoginUtils.isGuest()){
					return Result.error("login-required","请先登录，再切换账户");
				}
				password= initRandPassword(sequenceService.getTxFlowNo());
				tokenCacheService.put(ObjectTools.isEmpty(userloginid)?userid:userloginid,password);
			}

			if(StringUtils.hasText(registrationId)){
				client=clientRegistrationRepository.findByRegistrationId(registrationId);
			}
			if(client==null){
				return Result.error("client-not-exists","客户端不存在");
			}

			ClientRegistration finalClient = client;
			Map<String,Object> params=new HashMap<>();
			params.put("userid",userid);//userid,如果明确知道是userid,则取userid，userid全局唯一
			params.put("userloginid",userloginid);//userid,openid,phoneno,email等
			params.put("password",password);//密码，短信验证码，微信登陆时对应的code等
			params.put("authType",authType);//passwod，sms,email,wechat_mini_app等
			params.put("grantType",client.getAuthorizationGrantType().getValue());
			params.put("userType",userType);
			params.put("deptid",paramsVo.getDeptid());//部门号 可空
			params.put("branchId",paramsVo.getBranchId());//机构号 可空
			params.put("gloNo", LogUtils.getGloNo(true));//全局跟踪号


			Map messages = webClient
					.post()
					.uri(client.getProviderDetails().getTokenUri()+"?grant_type={grantType}&auth_type={authType}&user_type={userType}&userloginid={userloginid}&userid={userid}&password={password}&deptid={deptid}&branchId={branchId}&gloNo={gloNo}",params)
					.headers(h->{
						h.add("Authorization","Basic "+ Base64.getEncoder().encodeToString((finalClient.getClientId()+":"+ finalClient.getClientSecret()).getBytes(Charset.forName("UTF-8"))));
					})
					.retrieve()
					.bodyToMono(Map.class)
					.block();
			OAuth2AccessTokenResponse response=converter.convert(messages);
			Instant instant= (Instant) response.getAccessToken().getExpiresAt();
			Result result=Result.ok().setData(response);
			result.put("expiresAt",instant.toEpochMilli());
			tips = jwtLoginService.doLocalLoginUseJwtToken(response.getAccessToken().getTokenValue());
			User user=LoginUtils.getCurrentUserInfo();
			result.put("userInfo",user);

			CommonUserDetails userDetails=jwtLoginService.getUserDetails();
			result.put("roles",userDetails.getAuthorities());
			pushLoginRecord(paramsVo,user);
			return result;
		}catch (WebClientResponseException.Unauthorized e1){
			logger.error("账户密码错误",e1);
			List<String> errmsgHeader=e1.getHeaders().get("errmsg");
			if(errmsgHeader!=null && errmsgHeader.size()>0){
				String errmsg= errmsgHeader.get(0);
				int indexOf=errmsg.indexOf("lock-type-");
				if(StringUtils.hasText(errmsg) && indexOf>=0){
					String errcode=errmsg.substring(indexOf+10,indexOf+11);
					tips.setErrMsg("lock-type-"+errcode,LockUtil.getLockMsgByLockType(errcode));
					tips.put("moreMsg",errmsg);
				}else{
					tips.setErrMsg("account-id-or-password-err","账户或者密码错误");
					tips.put("moreMsg",errmsg);
				}
			}else{
				tips.setErrMsg("account-id-or-password-err","账户或者密码错误");
				tips.put("moreMsg",e1.getMessage());
			}
			return Result.build(LangTips.fromTips(tips));
		}catch (WebClientResponseException.InternalServerError e1){
			logger.error("服务器错误",e1);
			pushLoginErrRecord(paramsVo,"服务器错误");
			return Result.error("server-error","服务器错误").put("moreMsg",e1.getMessage());
		}catch (WebClientResponseException.BadGateway e1){
			logger.error("网关错误",e1);
			pushLoginErrRecord(paramsVo,"网关错误");
			return Result.error("bad-gateway","网关错误").put("moreMsg",e1.getMessage());
		}catch (WebClientResponseException.BadRequest e1){
			logger.error("不正确的请求",e1);
			pushLoginErrRecord(paramsVo,"不正确的请求");
			return Result.error("bad-request","不正确的请求").put("moreMsg",e1.getMessage());
		}catch (Exception e1){
			logger.error("获取令牌错误",e1);
			pushLoginErrRecord(paramsVo,"获取令牌错误");
			return Result.error("get-ticket-error","获取令牌错误").put("moreMsg",e1.getMessage());
		}

	}
	public String initRandPassword(String rawPassword){

		PasswordEncoder passwordEncoder=new SafePasswordEncoder();
		String passwordJsMd5= DigestUtils.md5DigestAsHex(rawPassword.getBytes());
		String passwordJavaMd5=passwordEncoder.encode(passwordJsMd5);
		return passwordJavaMd5;
	}

	private void pushLoginErrRecord(RequestTokenParams paramsVo,String errMsg){
		try {
		HttpServletRequest request=RequestUtils.getRequest();
		String loginIp=RequestUtils.getIpAddr(request);
		Map<String,Object> loginRecord=new HashMap<>();
		String userid=paramsVo.getUserloginid();
		loginRecord.put("reqNo", MDC.get("gloNo"));
		loginRecord.put("loginIp",loginIp);
		loginRecord.put("id",this.sequenceService.getTablePK("login_record","id"));
		loginRecord.put("loginTime",new Date());



		//获取浏览器信息
		String ua = request.getHeader("User-Agent");
		//转成UserAgent对象
		UserAgent userAgent = UserAgent.parseUserAgentString(ua);
		//获取浏览器信息
		Browser browser = userAgent.getBrowser();
		//获取系统信息
		OperatingSystem os = userAgent.getOperatingSystem();
		//系统名称
		String system = os.getName();
		//浏览器名称

		Version borderVersion= browser.getVersion(ua);
		if(borderVersion!=null){
			loginRecord.put("borderVersion",borderVersion.getVersion());
		}
		String browserName = browser.getName();
		loginRecord.put("userAgent",ua);
		loginRecord.put("userid",paramsVo.getUserloginid());
		if("sms".equals(paramsVo.getAuthType())){
			loginRecord.put("phoneno",paramsVo.getUserloginid());
		}
		loginRecord.put("os",os.getName());
		loginRecord.put("borderName",browserName);
		loginRecord.put("browerGroup",browser.getGroup().getName());
		loginRecord.put("renderingEngine",browser.getRenderingEngine().name());
		loginRecord.put("deviceManufacturer",os.getManufacturer().name());
		loginRecord.put("deviceType",os.getDeviceType().name());
		loginRecord.put("loginBranchId", paramsVo.getBranchId());
		loginRecord.put("loginStatus","0");
		loginRecord.put("loginMsg",errMsg);
		loginRecord.put("authType",paramsVo.getAuthType());
		loginRecord.put("grantType",paramsVo.getGrantType());
		loginRecordService.pushLoginRecord(loginRecord);


	}catch (Exception e){

	}
	}

	private void pushLoginRecord(RequestTokenParams paramsVo,User user) {
		try {
		HttpServletRequest request=RequestUtils.getRequest();
		String loginIp=RequestUtils.getIpAddr(request);
		Map<String,Object> loginRecord=new HashMap<>();
		loginRecord.putAll(user);
		loginRecord.put("reqNo", MDC.get("gloNo"));
		loginRecord.put("loginIp",loginIp);
		loginRecord.put("id",this.sequenceService.getTablePK("login_record","id"));
		loginRecord.put("loginTime",new Date());



		//获取浏览器信息
		String ua = request.getHeader("User-Agent");
		//转成UserAgent对象
		UserAgent userAgent = UserAgent.parseUserAgentString(ua);
		//获取浏览器信息
		Browser browser = userAgent.getBrowser();
		//获取系统信息
		OperatingSystem os = userAgent.getOperatingSystem();
		//系统名称
		String system = os.getName();
		//浏览器名称

		Version borderVersion= browser.getVersion(ua);
		if(borderVersion!=null){
			loginRecord.put("borderVersion",borderVersion.getVersion());
		}
		String browserName = browser.getName();
		loginRecord.put("userAgent",ua);
		loginRecord.put("os",os.getName());
		loginRecord.put("borderName",browserName);
		loginRecord.put("browerGroup",browser.getGroup().getName());
		loginRecord.put("renderingEngine",browser.getRenderingEngine().name());
		loginRecord.put("deviceManufacturer",os.getManufacturer().name());
		loginRecord.put("deviceType",os.getDeviceType().name());
		loginRecord.put("loginBranchId",user.getBranchId());
		loginRecord.put("memType",user.getMemType());
		loginRecord.put("loginShopId",user.getShopId());
		loginRecord.put("loginLocationId",user.getLocationId());
		loginRecord.put("loginStatus","1");
		loginRecord.put("loginMsg","成功");
		loginRecord.put("authType",paramsVo.getAuthType());
		loginRecord.put("grantType",paramsVo.getGrantType());
		loginRecordService.pushLoginRecord(loginRecord);


		}catch (Exception e){

		}
	}
}

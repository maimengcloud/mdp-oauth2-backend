package com.mdp.oauth2.client.user.entity;

import java.util.List;

public class RequestUserInfoParams {
    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    List<String> scopes;

}

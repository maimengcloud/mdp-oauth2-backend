package com.mdp.oauth2.client.user.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.mdp.core.utils.BaseUtils;
import com.mdp.safe.client.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.mdp.core.service.BaseService;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;

import com.mdp.oauth2.client.user.entity.MemMember;
import com.mdp.oauth2.client.user.mapper.MemMemberMapper;
/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com  顶级模块 mdp 大模块 oauth2.client.user 小模块 <br>
 * 实体 MemMember 表 mem_member 当前主键(包括多主键): userid; 
 ***/
@DS("mem-ds")
@Service
public class MemMemberService extends BaseService<MemMemberMapper,MemMember> {
	static Logger logger =LoggerFactory.getLogger(MemMemberService.class);


	public int userRegister(User user) {
		return insert(BaseUtils.fromMap(user,MemMember.class));
	}


	public int userUpdate(User user) {
		return super.updateById(BaseUtils.fromMap(user,MemMember.class))?1:0;
	}

	public int updatePassword(User user) {
		return getBaseMapper().updateUserPassword(user);
	}


	public int resetPasswordByPhoneno(User user) {
		return getBaseMapper().updateUserPassword(user);
	}


	public int resetPasswordByEmail(User user) {
		return getBaseMapper().updateUserPassword(user);

	}

	public List<User> queryByUserloginid(User params){

		return getBaseMapper().queryByUserloginid(params);
	}

	public List<User> queryMyUsers(User params){

		return getBaseMapper().queryMyUsers(params);
	}


	public void userUnregister(User user) {
		getBaseMapper().unregister(user);
	}

	public Tips updateOrgEmail(Map<String,Object> params){
		Tips tips = new Tips();
		getBaseMapper().updateOrgEmail(params);
		return tips;
	}
}


package com.mdp.oauth2.client.user.ctrl;

import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.safe.client.entity.*;
import com.mdp.safe.client.service.remote.UserResourceRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserResourceController  {

    @Autowired
    UserResourceRemoteService userResourceService;

    @GetMapping(value = "/user/resource/getRoleQxs")
    @ResponseBody public Result getRoleQxsFromRemote(@RequestParam String roleid) {
        Map<String,Qx> qxMap= userResourceService.getRoleQxsFromRemote(roleid);
        return Result.ok().setData(qxMap);
    }

    @GetMapping(value = "/user/resource/getRole")
    @ResponseBody public Result getRoleFromRemote(@RequestParam String roleid) {
        Role role = userResourceService.getRoleFromRemote(roleid);
        return Result.ok().setData(role);
    }

    @GetMapping(value = "/user/resource/getDept")
    @ResponseBody public Result getDeptFromRemote(@RequestParam String deptid) {
        Map<String,Object> result=new HashMap<>();
        Dept dept = userResourceService.getDeptFromRemote(deptid);
        return Result.ok().setData(dept);
    }

    @GetMapping(value = "/user/resource/getBranch")
    @ResponseBody public Result getBranchFromRemote(@RequestParam String branchId) {
        Map<String,Object> result=new HashMap<>();
        Branch branch = userResourceService.getBranchFromRemote(branchId);
        return Result.ok().setData(branch);
    }

    @GetMapping(value = "/user/resource/getRoleMenus")
    @ResponseBody public Result getRoleMenusFromRemote(@RequestParam String roleid) {
        Map<String,Object> result=new HashMap<>();
        Map<String,Menu> menuMap= userResourceService.getRoleMenusFromRemote(roleid);
        return Result.ok().setData(menuMap);
    }
    @GetMapping(value = "/user/resource/getPosts")
    @ResponseBody public Result getPostsFromRemote(@RequestParam String userid) {  
        return Result.ok().setData(userResourceService.getPostsFromRemote(userid));
    }
}

package com.mdp;

import com.mdp.oauth2.client.user.service.Oauth2EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * ActivityRewardRecordService的测试案例
 * 组织 com<br>
 * 顶级模块 mk<br>
 * 大模块 acti<br>
 * 小模块 <br>
 * 表 MK.acti_activity_reward_record 活动奖励规则表<br>
 * 实体 ActivityRewardRecord<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	actiId,id,rewardType,rewardEntityId,rewardEntityNum,rewardDesc,entityFp,entityShopId,entityLocationId,entitySp,userid,username,receiveStatus,outBizId,sentStatus,currentPrice,createTime,remainNumber,lockNo,lockStatus,goodsId,actiScene;<br>
 * 当前表的所有字段名:<br>
 *	acti_id,id,reward_type,reward_entity_id,reward_entity_num,reward_desc,entity_fp,entity_shop_id,entity_location_id,entity_sp,userid,username,receive_status,out_biz_id,sent_status,current_price,create_time,remain_number,lock_no,lock_status,goods_id,acti_scene;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestEmailService {

	@Autowired
    Oauth2EmailService emailService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void sendSimpleMail() {
		emailService.sendSimpleMail("测试发送邮件","cyc58469@163.com","cyc58469@163.com","测试邮件");
	}
	 
}

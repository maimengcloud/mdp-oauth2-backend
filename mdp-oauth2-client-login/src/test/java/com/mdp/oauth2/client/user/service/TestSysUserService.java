package  com.mdp.oauth2.client.user.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.mdp.oauth2.client.user.service.SysUserService;
import  com.mdp.oauth2.client.user.entity.SysUser;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * SysUserService的测试案例
 * 组织 com<br>
 * 顶级模块 mdp<br>
 * 大模块 oauth2.client.user<br>
 * 小模块 <br>
 * 表 sys_user 用户表<br>
 * 实体 SysUser<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	unionid,displayUserid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,districtId,email,fgOne,fgTwo,fgThr,idCardNo,officePhoneno,bizProcInstId,bizFlowState,memType,orgId,emailBak,pwdStrong,lockType,lockRemark,ltime,atype,branchId,continent,cpaType,cpaOrg,roleids,birthday,shopId,profeId,profeName,gradeId,gradeName,ilvlId,ilvlName,istatus,istime,ietime,validLvls,features,profeType,ustatus,creditId,creditScore,guardId,open,remark,skills,bizHours;<br>
 * 当前表的所有字段名:<br>
 *	unionid,display_userid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,district_id,email,fg_one,fg_two,fg_thr,id_card_no,office_phoneno,biz_proc_inst_id,biz_flow_state,mem_type,org_id,email_bak,pwd_strong,lock_type,lock_remark,ltime,atype,branch_id,continent,cpa_type,cpa_org,roleids,birthday,shop_id,profe_id,profe_name,grade_id,grade_name,ilvl_id,ilvl_name,istatus,istime,ietime,valid_lvls,features,profe_type,ustatus,credit_id,credit_score,guard_id,open,remark,skills,biz_hours;<br>
 * 当前主键(包括多主键):<br>
 *	userid;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSysUserService  {

	@Autowired
	SysUserService sysUserService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("unionid","4CDo","displayUserid","BySX","userid","Y6Ut","locked","C","startdate",new Date("2023-07-22 3:41:32"),"nickname","SUci","username","Bicu","phoneno","tm1l","password","9VC6","salt","455y","pwdtype","c","headimgurl","w97G","country","GhC6","city","fQHg","province","39Q4","address","n8X0","sex","cA43","enddate",new Date("2023-07-22 3:41:32"),"districtId","YNC6","email","695j","fgOne","wk11","fgTwo","vGR4","fgThr","NceG","idCardNo","f55F","officePhoneno","Cco0","bizProcInstId","n5dW","bizFlowState","M","memType","3","orgId","k073","emailBak","18lN","pwdStrong","9","lockType","5","lockRemark","TX4m","ltime",new Date("2023-07-22 3:41:32"),"atype","l","branchId","0y20","continent","ZHFN","cpaType","m","cpaOrg","1","roleids","INOK","birthday",new Date("2023-07-22 3:41:32"),"shopId","Ttfq","profeId","Y5oX","profeName","ye2s","gradeId","Wspf","gradeName","87ZM","ilvlId","N7Vf","ilvlName","9Vpq","istatus","4","istime",new Date("2023-07-22 3:41:32"),"ietime",new Date("2023-07-22 3:41:32"),"validLvls","5mcg","features","0G6x","profeType","7Nh3","ustatus","X","creditId","N38J","creditScore",7912,"guardId","I","open","i","remark","Du2f","skills","2xwn","bizHours","MO0C");
		SysUser sysUser=BaseUtils.fromMap(p,SysUser.class);
		sysUserService.save(sysUser);
		//Assert.assertEquals(1, result);
	}
	 
}

package  com.mdp.oauth2.client.user.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.mdp.oauth2.client.user.service.MemMemberService;
import  com.mdp.oauth2.client.user.entity.MemMember;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * MemMemberService的测试案例
 * 组织 com<br>
 * 顶级模块 mdp<br>
 * 大模块 oauth2.client.user<br>
 * 小模块 <br>
 * 表 mem_member 用户表<br>
 * 实体 MemMember<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	unionid,displayUserid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,districtId,email,fgOne,fgTwo,fgThr,idCardNo,officePhoneno,bizProcInstId,bizFlowState,memType,orgId,emailBak,pwdStrong,lockType,lockRemark,ltime,atype,branchId,continent,cpaType,cpaOrg,roleids,birthday,shopId,profeId,profeName,gradeId,gradeName,ilvlId,ilvlName,istatus,istime,ietime,validLvls,features,profeType,ustatus,creditId,creditScore,guardId,open,remark,skills,bizHours;<br>
 * 当前表的所有字段名:<br>
 *	unionid,display_userid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,district_id,email,fg_one,fg_two,fg_thr,id_card_no,office_phoneno,biz_proc_inst_id,biz_flow_state,mem_type,org_id,email_bak,pwd_strong,lock_type,lock_remark,ltime,atype,branch_id,continent,cpa_type,cpa_org,roleids,birthday,shop_id,profe_id,profe_name,grade_id,grade_name,ilvl_id,ilvl_name,istatus,istime,ietime,valid_lvls,features,profe_type,ustatus,credit_id,credit_score,guard_id,open,remark,skills,biz_hours;<br>
 * 当前主键(包括多主键):<br>
 *	userid;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestMemMemberService  {

	@Autowired
	MemMemberService memMemberService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("unionid","qDe9","displayUserid","ae6G","userid","Uvuz","locked","d","startdate",new Date("2023-07-22 8:8:22"),"nickname","Af48","username","5S5n","phoneno","CD9p","password","6380","salt","IdDn","pwdtype","G","headimgurl","0mpV","country","av0P","city","RWlC","province","ojDJ","address","5uXe","sex","dL0g","enddate",new Date("2023-07-22 8:8:22"),"districtId","qhZl","email","s9oo","fgOne","kH6j","fgTwo","UW5z","fgThr","X3CP","idCardNo","vC6m","officePhoneno","4Jnl","bizProcInstId","T28G","bizFlowState","J","memType","L","orgId","Wesk","emailBak","d19X","pwdStrong","l","lockType","J","lockRemark","l2t3","ltime",new Date("2023-07-22 8:8:22"),"atype","1","branchId","66J2","continent","mUOw","cpaType","H","cpaOrg","4","roleids","Wyhv","birthday",new Date("2023-07-22 8:8:22"),"shopId","DUSD","profeId","nG3m","profeName","nDQo","gradeId","UJpS","gradeName","MC9k","ilvlId","iZac","ilvlName","9vNT","istatus","Z","istime",new Date("2023-07-22 8:8:22"),"ietime",new Date("2023-07-22 8:8:22"),"validLvls","7J3Y","features","3ugf","profeType","c","ustatus","E","creditId","3ten","creditScore",4441,"guardId","i","open","G","remark","2j5a","skills","9aq1","bizHours","Iht0");
		MemMember memMember=BaseUtils.fromMap(p,MemMember.class);
		memMemberService.save(memMember);
		//Assert.assertEquals(1, result);
	}
	 
}

package  com.mdp.oauth2.server.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.mdp.oauth2.server.service.OauthRegisteredClientService;
import  com.mdp.oauth2.server.entity.OauthRegisteredClient;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * OauthRegisteredClientService的测试案例
 * 组织 com<br>
 * 顶级模块 mdp<br>
 * 大模块 oauth2.server<br>
 * 小模块 <br>
 * 表 oauth_registered_client oauth_registered_client<br>
 * 实体 OauthRegisteredClient<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	clientId,resourceIds,clientSecret,scopes,authorizationGrantTypes,redirectUris,authorities,autoapprove,id,requireProofKey,requireUserConsent,accessTokenTimeToLive,enableRefreshTokens,reuseRefreshTokens,refreshTokenTimeToLive,authenticationMethod;<br>
 * 当前表的所有字段名:<br>
 *	client_id,resource_ids,client_secret,scopes,authorization_grant_types,redirect_uris,authorities,autoapprove,id,require_proof_key,require_user_consent,access_token_time_to_live,enable_refresh_tokens,reuse_refresh_tokens,refresh_token_time_to_live,authentication_method;<br>
 * 当前主键(包括多主键):<br>
 *	client_id;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestOauthRegisteredClientService  {

	@Autowired
	OauthRegisteredClientService oauthRegisteredClientService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("clientId","HRB5","resourceIds","3DRt","clientSecret","38U7","scopes","LDi1","authorizationGrantTypes","JgLo","redirectUris","Y9aD","authorities","TF05","autoapprove","7X1K","id","WQ5g","requireProofKey","A","requireUserConsent","3","accessTokenTimeToLive",6483,"enableRefreshTokens","J","reuseRefreshTokens","D","refreshTokenTimeToLive",7479,"authenticationMethod","0QYF");
		OauthRegisteredClient oauthRegisteredClient=BaseUtils.fromMap(p,OauthRegisteredClient.class);
		oauthRegisteredClientService.save(oauthRegisteredClient);
		//Assert.assertEquals(1, result);
	}
	 
}

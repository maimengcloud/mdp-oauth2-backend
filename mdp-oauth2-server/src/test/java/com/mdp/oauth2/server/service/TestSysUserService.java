package  com.mdp.oauth2.server.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.mdp.oauth2.server.service.SysUserService;
import  com.mdp.oauth2.server.entity.SysUser;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * SysUserService的测试案例
 * 组织 com<br>
 * 顶级模块 mdp<br>
 * 大模块 oauth2.server<br>
 * 小模块 <br>
 * 表 sys_user 用户表<br>
 * 实体 SysUser<br>
 * 表是指数据库结构中的表,实体是指java类型中的实体类<br>
 * 当前实体所有属性名:<br>
 *	unionid,displayUserid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,districtId,email,fgOne,fgTwo,fgThr,idCardNo,officePhoneno,bizProcInstId,bizFlowState,memType,orgId,emailBak,pwdStrong,lockType,lockRemark,ltime,atype,branchId,continent,cpaType,cpaOrg,roleids,birthday,shopId,profeId,profeName,gradeId,gradeName,ilvlId,ilvlName,istatus,istime,ietime,validLvls,features,profeType,ustatus,creditId,creditScore,guardId,open,remark,skills,bizHours;<br>
 * 当前表的所有字段名:<br>
 *	unionid,display_userid,userid,locked,startdate,nickname,username,phoneno,password,salt,pwdtype,headimgurl,country,city,province,address,sex,enddate,district_id,email,fg_one,fg_two,fg_thr,id_card_no,office_phoneno,biz_proc_inst_id,biz_flow_state,mem_type,org_id,email_bak,pwd_strong,lock_type,lock_remark,ltime,atype,branch_id,continent,cpa_type,cpa_org,roleids,birthday,shop_id,profe_id,profe_name,grade_id,grade_name,ilvl_id,ilvl_name,istatus,istime,ietime,valid_lvls,features,profe_type,ustatus,credit_id,credit_score,guard_id,open,remark,skills,biz_hours;<br>
 * 当前主键(包括多主键):<br>
 *	userid;<br>
 ***/

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSysUserService  {

	@Autowired
	SysUserService sysUserService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("unionid","Q7Nd","displayUserid","QkzM","userid","hPP5","locked","X","startdate",new Date("2023-07-22 21:10:51"),"nickname","44R0","username","Oa15","phoneno","w0sx","password","Q7P4","salt","cOld","pwdtype","8","headimgurl","zoO3","country","hOjt","city","0xJA","province","3Ngm","address","dEk4","sex","Cm02","enddate",new Date("2023-07-22 21:10:51"),"districtId","eMeO","email","OOFf","fgOne","xia0","fgTwo","mpAN","fgThr","BkV0","idCardNo","Rslw","officePhoneno","qmP3","bizProcInstId","QSIB","bizFlowState","Z","memType","3","orgId","1c02","emailBak","va7d","pwdStrong","n","lockType","Y","lockRemark","TOj7","ltime",new Date("2023-07-22 21:10:51"),"atype","w","branchId","x12I","continent","5D4r","cpaType","9","cpaOrg","8","roleids","OcNL","birthday",new Date("2023-07-22 21:10:51"),"shopId","rGPi","profeId","LDKb","profeName","VoDK","gradeId","qG2F","gradeName","x3rQ","ilvlId","Tmc3","ilvlName","nxPO","istatus","c","istime",new Date("2023-07-22 21:10:51"),"ietime",new Date("2023-07-22 21:10:51"),"validLvls","fA6n","features","OTKM","profeType","10Th","ustatus","8","creditId","Ypgi","creditScore",8062,"guardId","0","open","N","remark","RAd2","skills","7HK9","bizHours","h38D");
		SysUser sysUser=BaseUtils.fromMap(p,SysUser.class);
		sysUserService.save(sysUser);
		//Assert.assertEquals(1, result);
	}
	 
}

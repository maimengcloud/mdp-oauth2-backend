package org.springframework.security.core.userdetails.cache;

import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 增加缓存，暂时不知道在哪里配置，先覆盖代码
 */
public class NullUserCache implements UserCache {


    public NullUserCache() {
    }

    public UserDetails getUserFromCache(String username) {
        return null;
     }

    public void putUserInCache(UserDetails user) {

    }

    public void removeUserFromCache(String username) {
     }

   public static void clear(){
    }

}

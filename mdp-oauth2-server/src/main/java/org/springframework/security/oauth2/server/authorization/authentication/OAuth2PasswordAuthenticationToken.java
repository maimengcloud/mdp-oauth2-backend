//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.springframework.security.oauth2.server.authorization.authentication;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.authorization.Version;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.util.Assert;

public class OAuth2PasswordAuthenticationToken extends AbstractAuthenticationToken {
    private static final long serialVersionUID;
    private UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
    OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken;


    public OAuth2PasswordAuthenticationToken(OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken,UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) {
        this(oAuth2ClientAuthenticationToken,usernamePasswordAuthenticationToken, Collections.emptySet());
    }

    public OAuth2PasswordAuthenticationToken(OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken,UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken, Set<String> scopes) {
        super(Collections.emptyList());
        Assert.notNull(usernamePasswordAuthenticationToken, "clientPrincipal cannot be null");
        Assert.notNull(scopes, "scopes cannot be null");
        this.oAuth2ClientAuthenticationToken=oAuth2ClientAuthenticationToken;
        this.usernamePasswordAuthenticationToken = usernamePasswordAuthenticationToken;
    }

    public Object getPrincipal() {
        return this.usernamePasswordAuthenticationToken;
    }

    public Object getCredentials() {
        return "";
    }

    public UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken() {
        return usernamePasswordAuthenticationToken;
    }

    public void setUsernamePasswordAuthenticationToken(UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) {
        this.usernamePasswordAuthenticationToken = usernamePasswordAuthenticationToken;
    }

    public OAuth2ClientAuthenticationToken getOAuth2ClientAuthenticationToken() {
        return oAuth2ClientAuthenticationToken;
    }

    public void setOAuth2ClientAuthenticationToken(OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken) {
        this.oAuth2ClientAuthenticationToken = oAuth2ClientAuthenticationToken;
    }

    static {
        serialVersionUID = Version.SERIAL_VERSION_UID;
    }

}

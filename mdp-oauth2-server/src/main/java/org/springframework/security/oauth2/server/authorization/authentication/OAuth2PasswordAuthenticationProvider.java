//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.springframework.security.oauth2.server.authorization.authentication;

import java.util.Set;

import com.mdp.core.SpringUtils;
import com.mdp.safe.client.entity.CommonUserDetails;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.keys.KeyManager;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType;
import org.springframework.security.oauth2.jose.jws.NimbusJwsEncoder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationAttributeNames;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.token.OAuth2Tokens;
import org.springframework.util.Assert;

public class OAuth2PasswordAuthenticationProvider implements AuthenticationProvider {
    private final OAuth2AuthorizationService authorizationService;
    private final JwtEncoder jwtEncoder;

    public OAuth2PasswordAuthenticationProvider(OAuth2AuthorizationService authorizationService) {
        Assert.notNull(authorizationService, "authorizationService cannot be null");
        this.authorizationService = authorizationService;
        NimbusJwsEncoder jwtEncoder = new NimbusJwsEncoder( SpringUtils.getBean(KeyManager.class));
        this.jwtEncoder = jwtEncoder;
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        OAuth2PasswordAuthenticationToken oAuth2PasswordAuthenticationToken = (OAuth2PasswordAuthenticationToken)authentication;
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken= (UsernamePasswordAuthenticationToken) oAuth2PasswordAuthenticationToken.getPrincipal();
        RegisteredClient registeredClient = oAuth2PasswordAuthenticationToken.getOAuth2ClientAuthenticationToken().getRegisteredClient();
        if (!registeredClient.getAuthorizationGrantTypes().contains(AuthorizationGrantType.PASSWORD)) {
            throw new OAuth2AuthenticationException(new OAuth2Error("unauthorized_client"));
        } else {
            Set<String> scopes = registeredClient.getScopes();
            Jwt jwt = OAuth2TokenIssuerUtil.issueJwtAccessToken(this.jwtEncoder, usernamePasswordAuthenticationToken, registeredClient.getClientId(), (Set)scopes);
            OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, jwt.getTokenValue(), jwt.getIssuedAt(), jwt.getExpiresAt(), (Set)scopes);
            OAuth2Authorization authorization = OAuth2Authorization.withRegisteredClient(registeredClient).principalName(usernamePasswordAuthenticationToken.getName()).tokens(OAuth2Tokens.builder().accessToken(accessToken).build()).attribute(OAuth2AuthorizationAttributeNames.ACCESS_TOKEN_ATTRIBUTES, jwt).build();
            this.authorizationService.save(authorization);
            return new OAuth2AccessTokenAuthenticationToken(registeredClient, usernamePasswordAuthenticationToken, accessToken);
        }
    }

    public boolean supports(Class<?> authentication) {
        return OAuth2PasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.springframework.security.web.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

public final class HttpStatusEntryPoint implements AuthenticationEntryPoint {
    private final HttpStatus httpStatus;

    public HttpStatusEntryPoint(HttpStatus httpStatus) {
        Assert.notNull(httpStatus, "httpStatus cannot be null");
        this.httpStatus = httpStatus;
    }

    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        response.setStatus(this.httpStatus.value());
        if(authException.getMessage()!=null && authException.getMessage().indexOf("lock-type-")>=0){
            response.setHeader("errmsg",authException.getMessage());
        }

        //response.setStatus(this.httpStatus.value(),authException.getMessage());
    }
}

package  com.mdp.oauth2.server.entity;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com  顶级模块 mdp 大模块 oauth2.server  小模块 <br> 
 * 实体 OauthRegisteredClient所有属性名: <br>
 *	"clientId","client_id","resourceIds","resource_ids","clientSecret","client_secret","scopes","scopes","authorizationGrantTypes","authorization_grant_types","redirectUris","redirect_uris","authorities","authorities","autoapprove","autoapprove","id","id","requireProofKey","require_proof_key","requireUserConsent","require_user_consent","accessTokenTimeToLive","access_token_time_to_live","enableRefreshTokens","enable_refresh_tokens","reuseRefreshTokens","reuse_refresh_tokens","refreshTokenTimeToLive","refresh_token_time_to_live","authenticationMethod","basic\post\none";<br>
 * 当前主键(包括多主键):<br>
 *	client_id;<br>
 */
 @Data
@TableName("oauth_registered_client")
@ApiModel(description="oauth_registered_client")
public class OauthRegisteredClient  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(notes="client_id,主键",allowEmptyValue=true,example="",allowableValues="")
	String clientId;
	
	@ApiModelProperty(notes="resource_ids",allowEmptyValue=true,example="",allowableValues="")
	String resourceIds;
	
	@ApiModelProperty(notes="client_secret",allowEmptyValue=true,example="",allowableValues="")
	String clientSecret;
	
	@ApiModelProperty(notes="scopes",allowEmptyValue=true,example="",allowableValues="")
	String scopes;
	
	@ApiModelProperty(notes="authorization_grant_types",allowEmptyValue=true,example="",allowableValues="")
	String authorizationGrantTypes;
	
	@ApiModelProperty(notes="redirect_uris",allowEmptyValue=true,example="",allowableValues="")
	String redirectUris;
	
	@ApiModelProperty(notes="authorities",allowEmptyValue=true,example="",allowableValues="")
	String authorities;
	
	@ApiModelProperty(notes="autoapprove",allowEmptyValue=true,example="",allowableValues="")
	String autoapprove;
	
	@ApiModelProperty(notes="id",allowEmptyValue=true,example="",allowableValues="")
	String id;
	
	@ApiModelProperty(notes="require_proof_key",allowEmptyValue=true,example="",allowableValues="")
	String requireProofKey;
	
	@ApiModelProperty(notes="require_user_consent",allowEmptyValue=true,example="",allowableValues="")
	String requireUserConsent;
	
	@ApiModelProperty(notes="access_token_time_to_live",allowEmptyValue=true,example="",allowableValues="")
	Integer accessTokenTimeToLive;
	
	@ApiModelProperty(notes="enable_refresh_tokens",allowEmptyValue=true,example="",allowableValues="")
	String enableRefreshTokens;
	
	@ApiModelProperty(notes="reuse_refresh_tokens",allowEmptyValue=true,example="",allowableValues="")
	String reuseRefreshTokens;
	
	@ApiModelProperty(notes="refresh_token_time_to_live",allowEmptyValue=true,example="",allowableValues="")
	Integer refreshTokenTimeToLive;
	
	@ApiModelProperty(notes="basic|post|none",allowEmptyValue=true,example="",allowableValues="")
	String authenticationMethod;

	/**
	 *client_id
	 **/
	public OauthRegisteredClient(String clientId) {
		this.clientId = clientId;
	}
    
    /**
     * oauth_registered_client
     **/
	public OauthRegisteredClient() {
	}

}
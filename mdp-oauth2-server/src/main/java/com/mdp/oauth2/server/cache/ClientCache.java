package com.mdp.oauth2.server.cache;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ClientCache {
    public  Map<String, RegisteredClient> clients=new ConcurrentHashMap<String, RegisteredClient>() ;
    public void put(RegisteredClient client){
        clients.put(client.getClientId(),client);
        clients.put(client.getId(),client);
    }
    public RegisteredClient getByClientId(String clientId){
        return  clients.get(clientId);
    }
    public RegisteredClient getById(String id){
        return  clients.get(id);
    }
    public void remove(String clientId){
        clients.remove(clientId);
    }
    @Scheduled(cron = "0 0 1,2 * * ?")
    public void clear(){
        clients.clear();
    }
}

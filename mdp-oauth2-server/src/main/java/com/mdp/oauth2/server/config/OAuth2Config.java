package com.mdp.oauth2.server.config;



import com.mdp.safe.client.pwd.SafePasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class OAuth2Config {



    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder passwordEncoder(){
        return new SafePasswordEncoder();
    };

    @Scheduled(cron = "0 0 1,2,3 * * ?")
    public void clearUserCahce(){
        NullUserCache.clear();
    }
}
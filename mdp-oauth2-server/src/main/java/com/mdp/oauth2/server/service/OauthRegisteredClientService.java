package com.mdp.oauth2.server.service;

import com.mdp.safe.client.entity.DeptPostRole;
import com.mdp.safe.client.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.mdp.core.service.BaseService;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;

import com.mdp.oauth2.server.entity.OauthRegisteredClient;
import com.mdp.oauth2.server.mapper.OauthRegisteredClientMapper;
/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com  顶级模块 mdp 大模块 oauth2.server 小模块 <br>
 * 实体 OauthRegisteredClient 表 oauth_registered_client 当前主键(包括多主键): client_id; 
 ***/
@Service
public class OauthRegisteredClientService extends BaseService<OauthRegisteredClientMapper,OauthRegisteredClient> {
	static Logger logger =LoggerFactory.getLogger(OauthRegisteredClientService.class);



}


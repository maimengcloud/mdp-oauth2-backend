package com.mdp.oauth2.server.service;

import com.mdp.core.utils.ObjectTools;
import com.mdp.safe.client.entity.DeptPostRole;
import com.mdp.safe.client.entity.Role;
import com.mdp.safe.client.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import com.mdp.core.service.BaseService;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;

import com.mdp.oauth2.server.entity.SysUser;
import com.mdp.oauth2.server.mapper.SysUserMapper;
import org.springframework.util.StringUtils;

/**
 * 父类已经支持增删改查操作,因此,即使本类什么也不写,也已经可以满足一般的增删改查操作了.<br> 
 * 组织 com  顶级模块 mdp 大模块 oauth2.server 小模块 <br>
 * 实体 SysUser 表 sys_user 当前主键(包括多主键): userid; 
 ***/
@Service
public class SysUserService extends BaseService<SysUserMapper,SysUser> {
	static Logger logger =LoggerFactory.getLogger(SysUserService.class);

	public List<User> queryByUserloginid(Map<String,Object> params) {
		List<User> users= baseMapper.queryByUserloginid(params);
		return users;
	}
	public User getUserByUserid( Map<String,Object> params) {

		List<User> users =baseMapper.queryByUserloginid(params);
		return users.size()>0?users.get(0):null;
	}
	public List<DeptPostRole> getUserDeptPostRoles(Map<String, Object> params) {

		return baseMapper.loadUserDeptPostRolesByUserid(params);
	}

    public SysUser queryOrgByOrgId(String orgId) {
		return baseMapper.selectById(orgId);
    }

    public static User autoTestUser(List<User> users,String userType,String branchId){
		if(users==null || users.size()==0){
			return null;
		}
		List<User> temps=users;
		List<User> usersFilterByBranchId= ObjectTools.isEmpty(branchId)?users:users.stream().filter(k->branchId.equals(k.getBranchId())).collect(Collectors.toList());
		if(usersFilterByBranchId!=null && usersFilterByBranchId.size()==1){
			return usersFilterByBranchId.get(0);
		}
		temps=usersFilterByBranchId.size()>0?usersFilterByBranchId:temps;
		List<User> defLogins=temps.stream().filter(k->"1".equals(k.get("defLogin"))).collect(Collectors.toList());

		if(defLogins!=null && defLogins.size()>0){
			if(defLogins.size()==1){
				return defLogins.get(0);
			}else if(defLogins.size()>1){
				temps=defLogins;
			}
		}

		User mainUser=null;
		User custUser=null;
		User staffUser=null;

		if(temps!=null && temps.size()>0){
			if(temps.size()==1){
				return temps.get(0);
			}else{
				for (User user : temps) {
					if("1".equals(user.getAtype())){
						mainUser=user;
					}
					if("2".equals(user.getMemType())){
						staffUser=user;
					}
					if("0".equals(user.getMemType())){
						custUser= user;
					}
				}
				if("staff".equals(userType)){
					if(staffUser!=null){
						return staffUser;
					}else if(mainUser!=null){
						return mainUser;
					}else if(custUser!=null){
						return custUser;
					}else{
						return temps.get(0);
					}
				} else{
					if(mainUser!=null){
						return mainUser;
					}else if(custUser!=null){
						return custUser;
					}else if(staffUser!=null){
						return staffUser;
					}else{
						return temps.get(0);
					}
				}
			}
		}else{
			return null;
		}
	}

	public Role getRole(String roleid) {
		if( !StringUtils.hasText(roleid) || roleid.startsWith("SCOPE_")){
			return null;
		}
		return baseMapper.getRole(roleid);
	}
}


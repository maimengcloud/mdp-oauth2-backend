package com.mdp.oauth2.server.service;

import com.mdp.core.err.BizException;
import com.mdp.oauth2.server.entity.SysUser;
import com.mdp.safe.client.entity.DeptPostRole;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.service.remote.DefaultUserBaseInfoRemoteQueryService;
import com.mdp.safe.client.service.remote.UserBaseInfoRemoteQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 覆盖用户信息查询基本信息接口，改由访问本地数据库
 * @see DefaultUserBaseInfoRemoteQueryService
 */
@Service
public class UserBaseInfoQueryService implements UserBaseInfoRemoteQueryService {

    @Autowired
    public SysUserService sysUserService;


    @Override
    public List<User> queryByUserloginid(String userloginid, String idType, Map<String, Object> extParams) {
        User user= new User();

        if(!StringUtils.hasText(idType)||"all".equals(idType)){
            user.put("userloginid",userloginid);
            user.put("idType","all");
        }else if("userid".equals(idType)){
            user.setUserid(userloginid);
        }else if("phoneno".equals(idType)){
            user.setPhoneno(userloginid);
        }else if("displayUserid".equals(idType)){
            user.setDisplayUserid(userloginid);
        }else if("email".equals(idType)){
            user.setEmail(userloginid);
        }else if("tpaOpenid".equals(idType)){
            user.setTpaOpenid(userloginid);
        }else if("unionid".equals(idType)){
            user.setUnionid(userloginid);
        }else{
            throw new BizException("idType-err","登录参数类型错误");
        }

        user.setUserType((String) extParams.get("userType"));
        if("staff".equals(user.getUserType())){
            return sysUserService.queryByUserloginid(user);
        }else {
            return sysUserService.queryByUserloginid(user);
        }

    }

    @Override
    public User getUserByUserid(String userid, Map<String, Object> extParams) {
        Map<String,Object> params=new HashMap<>();
        params.put("userid",userid);
        params.put("platformBranchId",getPlatformBranchId());
        String userType=this.getUserTypeFromParams(extParams);
        User user=null;
        if("staff".equals(userType)){
            user=this.sysUserService.getUserByUserid(params);
        }else {
            user=this.sysUserService.getUserByUserid(params);
        }
        setUserTypeToUser(user,extParams);
        return user;
    }


    @Override
    public List<DeptPostRole> getUserDeptPostRoles(String userid, Map<String, Object> extParams) {
        Map<String,Object> params = new HashMap<>();
        params.put("userid",userid);
        params.put("platformBranchId",getPlatformBranchId());
        if(extParams!=null){
            params.put("userType",extParams.get("userType"));
        }
        if(extParams!=null && extParams.containsKey("deptid")){
            params.put("deptid",extParams.get("deptid"));
        }
        return sysUserService.getUserDeptPostRoles(params);
    }

    public void setUserTypeToUser(User user,Map<String,Object> extParams){
        if(user==null){
            return;
        }
        if(extParams==null){
            user.setUserType("staff");
            return;
        }
        String userType= (String) extParams.get("userType");
        if(StringUtils.hasText(userType)){
            user.setUserType(userType);
        }else{
            user.setUserType("staff");
        }

    }

    public String getUserTypeFromParams(Map<String,Object> extParams){
        if(extParams==null){
            return "staff";
        }
        String userType= (String) extParams.get("userType");
        if(!StringUtils.hasText(userType)){
            return "staff";
        }
        return userType;
    }
    
    
    public String getPlatformBranchId(){
        return "platform-branch-001";
    }

    public SysUser queryOrgByOrgId(String orgId) {
        return this.sysUserService.queryOrgByOrgId(orgId);
    }
}

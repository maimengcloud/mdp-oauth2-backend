package com.mdp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * 启动类
 * @author  chenyc
 */
@EnableAsync
@SpringCloudApplication
public class LoginClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginClientApplication.class, args);
    }

}
